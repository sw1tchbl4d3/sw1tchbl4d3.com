from os import urandom
from datetime import date
import json

from flask import *

from highlighter import syntax_highlight

ACME_CHALL = ""

LEGACY_LOOKUP = {"htb": "/writeups/any/htb/", 
                 "cscg2020": "/writeups/2020/cscg/", 
                 "hacktivitycon": "/writeups/2020/hacktivitycon/"}

with open("routes.json", "r") as fd:
    ROUTES = json.load(fd)

def get_age():
    today = date.today()
    born = date(2004, 8, 7)
    age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
    return age
    
def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = urandom(128)

    # - BIN -

    if ACME_CHALL:
        @app.route(f"/.well-known/acme-challenge/{ACME_CHALL.split('.')[0]}")
        def wk_acme_challenge():
            response = make_response(ACME_CHALL, 200)
            response.mimetype = "text/plain"
            return response

    @app.route("/.well-known/security.txt")
    def wk_security():
        return send_from_directory(app.static_folder, "security.txt")

    @app.route("/.well-known/matrix/client")
    def wk_matrixclient():
        return {"m.homeserver": {"base_url": "https://matrix.sw1tchbl4d3.com"}}

    @app.route("/robots.txt")
    def robots():
        return send_from_directory(app.static_folder, "robots.txt")

    @app.route("/favicon.ico")
    def favicon():
        return send_from_directory(app.static_folder, "img/favicon-16.ico")

    @app.route("/pubkey.txt")
    def pubkey():
        return send_from_directory(app.static_folder, "pubkey.txt")

    @app.route("/security.txt")
    def security():
        return wk_security()

    # - PAGES - 

    @app.route("/")
    def index():
        return render_template("index.html", age=get_age())

    @app.route("/about")
    def about():
        return render_template("about.html", age=get_age())
    
    @app.route("/writeups/")
    def writeup_listing():
        return render_template("writeups/listing.html")
    
    @app.route("/writeups/<string:year>/<string:ctf>/")
    def ctf_listing(year, ctf):

        if year not in ROUTES.keys() or ctf not in ROUTES[year].keys():
            # this may still be a legacy "/writeups/<string:ctf>/<string:writeup>" link, so we check for that
            writeup = ctf
            ctf = year
            
            if year not in LEGACY_LOOKUP.keys():
                abort(404)
    
            return redirect(LEGACY_LOOKUP[year] + writeup, code=301)

        return render_template(f"writeups/{year}/{ctf}/listing.html")

    @app.route("/writeups/<string:year>/<string:ctf>/<string:writeup>")
    def writeup(year, ctf, writeup):
        START = '<pre class="codeblock_to_be_rendered">'
        NEWSTART = '<pre class="codeblock">'
        END = '</pre>'

        if year not in ROUTES.keys():
            abort(404)
        if ctf not in ROUTES[year].keys():
            abort(404)
        if writeup not in ROUTES[year][ctf]:
            abort(404)

        rendered = render_template(f"writeups/{year}/{ctf}/{writeup}.html")

        #FIXME: find a better way to pad this (css?)
        rendered = rendered.replace('<pre class="codeblock">', '<pre class="codeblock">\n')

        last_idx = 0
        while START in rendered:
            start_idx = rendered.index(START, last_idx)
            stop_idx = rendered.index(END, start_idx)
            last_idx = stop_idx

            codeblock = rendered[start_idx+len(START):stop_idx]

            rendered = rendered[:start_idx] + NEWSTART + syntax_highlight(codeblock) + rendered[stop_idx:]

        return rendered

    # - OLD REDIRECTS -

    @app.route("/writeups/<string:ctf>/")
    def legacy_ctf_only(ctf):
        if ctf not in LEGACY_LOOKUP.keys():
            abort(404)

        return redirect(LEGACY_LOOKUP[ctf], code=301)

    # - ERRORS - 

    @app.errorhandler(404)
    def err_404(e):
        return render_template("errors/404.html"), 404

    @app.errorhandler(401)
    def err_401(e):
        return render_template("errors/401.html"), 401

    return app
