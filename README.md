# sw1tchbl4d3.com

This is the source code of my personal website, sw1tchbl4d3.com

It lays focus on working without js, doing all of the legwork in the backend with flask.

For example, I replaced the js syntax highlighter of my old website with a selfmade one.

This repo accepts PRs of all kinds, no matter if they are color scheme changes I like or a patch for a vulnerability.

This repo does *not* contain the /static/files directory of my real website, as that would be too big to upload here.

(I already had my worries with the /static/img directory)

## Usage

You can pull this repo, install uwsgi, and run the following command to start a webserver on port 5000:

```sh
uwsgi --socket 127.0.0.1:5000 --wsgi-file start.py --master --processes 4 --threads 2
```

You can adjust the amount of processes and threads to your liking.
