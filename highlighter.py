from keyword import kwlist 
from shlex import quote as shell_escape
import string
import re

COLORS = {
    "NUMERIC_LITERALS": "#5bc0de",
    "STRING_LITERALS": "#d9984f",
    "FUNCTION_CALLS": "#d9534f",
    "KEYWORDS": "#428bca",
    "PRIMITIVES": "#5cb88a",
    "COMMENTS": "#5cb85c",
    "DEFAULT": "#ffffff"
}

def separate(value, separators):
    return re.split('([' + shell_escape(separators) + '])', value)

def colorize(snippet, color):
    return f"<span style=\"color: {color}\">{snippet}</span>"

def syntax_highlight(text):
    finished = ""

    # token separators, excluding _, as it can be part of variable and function names
    separators = (" " + string.punctuation + "\n").replace("_", "")

    tokens = separate(text, separators)

    quote_started = False
    comment_started = False
    escape_next = 0
    i = 0

    for token in tokens:
        if token == "\\":
            escape_next = 2

        if "\n" in token and comment_started:
            finished += colorize(token, COLORS["COMMENTS"])
            comment_started = False

        elif comment_started:
            finished += colorize(token, COLORS["COMMENTS"])

        # Quote detection, which also supports docstrings (token*3)
        elif token in ['"', "'"]:
            finished += colorize(token, COLORS["STRING_LITERALS"])
            if tokens[i-2].strip() == tokens[i-4].strip() == token and tokens[i-6].strip() != "\\":
                if quote_started == token*3:
                    quote_started = False
                else:
                    quote_started = token*3
            elif quote_started == token and escape_next < 0:
                quote_started = False
            elif quote_started == False:
                quote_started = token

        elif quote_started:
            finished += colorize(token, COLORS["STRING_LITERALS"])

        elif token == "#":
            if escape_next < 0:
                comment_started = True
                finished += colorize(token, COLORS["COMMENTS"])

        elif token in ["str", "bool", "int", "list", "dict"]:
            finished += colorize(token, COLORS["PRIMITIVES"])

        elif token in kwlist + ["self", "print"]:
            finished += colorize(token, COLORS["KEYWORDS"])

        elif token.isdigit():
            finished += colorize(token, COLORS["NUMERIC_LITERALS"])
        
        elif len(tokens)-1 > i and all([i in string.ascii_letters+string.digits+"_" for i in token]) and tokens[i+1] == "(":
            finished += colorize(token, COLORS["FUNCTION_CALLS"])
        
        # Don't spam <span>'s when the token is a space
        elif token in [" ", ""]:
            finished += token
        
        else:
            finished += colorize(token, COLORS["DEFAULT"])
        
        escape_next -= 1
        i += 1

    return finished
